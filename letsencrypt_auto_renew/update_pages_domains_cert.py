#!/usr/bin/env python3
# coding: utf-8
import requests,json

# Base URL
base_url = 'https://gitlab.com/api/v4/projects/'
private_token = ''
project_id = ''
domain = ''
url = base_url + project_id + '/pages/domains/' + domain
headers = {'Private-Token': private_token}

# load Certificate
certificate = open("/etc/letsencrypt/live/" + domain + "/fullchain.pem")
certificate_data = certificate.read()
key = open("/etc/letsencrypt/live/" + domain + "/privkey.pem")
key_data = key.read()
data = {'certificate': certificate_data, 'key': key_data}

# update Certificate
response_json = requests.put(url, headers=headers, data=data)
responce = json.loads(response_json.text)

# result
code = response_json.status_code
if code == 200:
  print("Done!")
else:
  print("Failed")
  print(response_json.text)
