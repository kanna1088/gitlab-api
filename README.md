# GitLab-API
## おしながき
- letsencrypt_auto_renew : GitLab PagesでLet's Encryptの更新を自動化する
- get_pages_domains.py : GitLab Pagesのプロジェクトに登録されたドメインの情報が見れる
****
## letsencrypt_auto_renew
GitLab PagesでのLet's Encryptの更新を自動化することができます。<br>
Linuxでrenew.bashを実行すると、更新の必要性を確認して、必要ならば更新してGitLab Pagesに反映させます。
※GitLab Pagesにドメインが登録されていないと更新させることはできません。
****
### 必要なこと
プログラム内の変数に指定が必要な項目があります。<br>
- renew.bash<br>
domain : 更新予定のドメインを指定してください。
- update_pages_domains_cert.py<br>
private_token : GitLabにて、Personal Access Tokensを発行して指定してください。Scopesはapiです。<br>
project_id : ドメインが登録されているプロジェクトidを指定してください。<br>
domain : 更新予定のドメインを指定してください。<br>

事前に証明書更新に利用するLinuxでcertbotによる証明書取得を行う必要があります。<br>

BaseURLのドメインをGitLab(gitlab.com)に指定しているので、自分で構築したサーバで利用する場合は、適宜変更してください。
****